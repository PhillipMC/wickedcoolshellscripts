#!/bin/bash

in_path() {
  # Given a command and the PATH, tries to find the command. Returns 0 if
  #   found and executable; 1 if not. Note that this temporarily modifies
  #   the IFS (internal field separator) but restores it upon completion.

  cmd=$1
  ourpath=$2
  result=1
  oldIFS=$IFS  
  IFS=":"

  for directory in $ourpath; do
    if [ -x "$directory/$cmd" ] ; then #AQUI ESTA EL FALLO Y NOSE PORQUE ES
      result=0      # If we're here, we found the command.
    fi
  done

  IFS=$oldIFS
  return $result
}
checkForCmdInPath() {
  var=$1

  if [ "$var" != "" ] ; then
     if [ "${var:0:1}" = "/" ] ; then
       if [ ! -x $var ] ; then
         return 1
       fi
     elif ! in_path $var "$PATH" ; then
       return 2
     fi
   fi
   
}


validAlphaNum() {
  # Validate arg: returns 0 if all upper+lower+digits; 1 otherwise
  # Remove all unacceptable chars.
  validchars="$(echo $1 | sed -e 's/[^[:alnum:]]//g')"
  echo $validchars
  if [ "$validchars" = "$1" ] ; then
    return 0
  else
    return 1
  fi
}


monthNumToName() {
  # Sets the 'month' variable to the appropriate value.
  case $1 in
    1 ) month="Ene";;
    2 ) month="Feb";;
    3 ) month="Mar";;
    4 ) month="Abr";;
    5 ) month="May";;
    6 ) month="Jun";;
    7 ) month="Jul";;
    8 ) month="Ago";;
    9 ) month="Sep";;
    10) month="Oct";;
    11) month="Nov";;
    12) month="Dic";;
    * ) echo "$0: Valor de mes desconocido $1" >&2
        exit 1
  esac
  return 0
}


nicenumber() {
  # Note that we assume that '.' is the decimal separator in the INPUT value
  #   to this script. The decimal separator in the output value is '.' unless
  #   specified by the user with the -d flag.

  integer=$(echo $1 | cut -d. -f1)        # Left of the decimal
  decimal=$(echo $1 | cut -d. -f2)        # Right of the decimal
  # Check if number has more than the integer part.
  if [ "$decimal" != "$1" ]; then
    # There's a fractional part, so let's include it.
    result="${DD:= '.'}$decimal"
  fi

  thousands=$integer
  while [ $thousands -gt 999 ]; do
    remainder=$(($thousands % 1000))    # Three least significant digits

    # We need 'remainder' to be three digits. Do we need to add zeros?
    while [ ${#remainder} -lt 3 ] ; do  # Force leading zeros
      remainder="0$remainder"
    done

    result="${TD:=","}${remainder}${result}"    # Builds right to left
    thousands=$(($thousands / 1000))    # To left of remainder, if any
  done

  nicenum="${thousands}${result}"
  if [ ! -z $2 ] ; then
    echo $nicenum
  fi
}


validint() {
    # Validate first field and test that value against min value $2 and/or
    # max value $3 if they are supplied. If the value isn't within range
    # or it's not composed of just digits, fail.
    number="$1";
    min="$2";
    max="$3"
    if [ -z $number ] ; then
        echo "You didn't enter anything. Please enter a number." >&2
        return 1
    fi
    
    # Is the first character a '-' sign?
    if [ "${number%${number#?}}" = "-" ] ; then
        testvalue="${number#?}" # Grab all but the first character to test.
    else
        testvalue="$number"
    fi
    
    # Create a version of the number that has no digits for testing.
    nodigits="$(echo $testvalue | sed 's/[[:digit:]]//g')"
    # Check for nondigit characters.
    if [ ! -z $nodigits ] ; then
        echo "Invalid number format! Only digits, no commas, spaces, etc." >&2
        return 1
    fi
    if [ ! -z $min ] ; then
        # Is the input less than the minimum value?
        if [ "$number" -lt "$min" ] ; then
            echo "Your value is too small: smallest acceptable value is $min." >&2
            return 1
        fi
    fi
    if [ ! -z $max ] ; then
        # Is the input greater than the maximum value?
        if [ "$number" -gt "$max" ] ; then
        echo "Your value is too big: largest acceptable value is $max." >&2
        return 1
        fi
    fi
    return 0
}


validfloat() {
    
    fvalue="$1"
    # Check whether the input number has a decimal point.
    if [ ! -z $(echo $fvalue | sed 's/[^.]//g') ] ; then
        # Extract the part before the decimal point.
        decimalPart="$(echo $fvalue | cut -d. -f1)"
        # Extract the digits after the decimal point.
        fractionalPart="${fvalue#*\.}"
        # Start by testing the decimal part, which is everything
        # to the left of the decimal point.
        if [ ! -z $decimalPart ] ; then
            # "!" reverses test logic, so the following is
            # "if NOT a valid integer"
            if ! validint "$decimalPart" "" "" ; then
                return 1
            fi
    fi

    # Now let's test the fractional value.
    # To start, you can't have a negative sign after the decimal point
    # like 33.-11, so let's test for the '-' sign in the decimal.
    if [ "${fractionalPart%${fractionalPart#?}}" = "-" ] ; then
        echo -e "Invalid floating-point number: '-' not allowed after decimal point." >&2
        return 1
    fi
    if [ "$fractionalPart" != "" ] ; then
        # If the fractional part is NOT a valid integer...
        if ! validint "$fractionalPart" "0" "" ; then
            return 1
        fi
    fi
    else
        # If the entire value is just "-", that's not good either.
        if [ "$fvalue" = "-" ] ; then
            echo "Invalid floating-point format." >&2
            return 1
        fi
    # Finally, check that the remaining digits are actually
    # valid as integers.
        if ! validint "$fvalue" "" "" ; then
            return 1
        fi
    fi

    echo "$1 is a valid floating-point value."
    return 0
}


exceedsDaysInMonth() {
    # Given a month name and day number in that month, this function will
    #    return 0 if the specified day value is less than or equal to the
    #    max days in the month; 1 otherwise.

    case $(echo $1|tr '[:upper:]' '[:lower:]') in
        ene* ) days=31    ;; feb* ) days=28    ;;
        mar* ) days=31    ;; abr* ) days=30    ;;
        may* ) days=31    ;; jun* ) days=30    ;;
        jul* ) days=31    ;; ago* ) days=31    ;;
        sep* ) days=30    ;; oct* ) days=31    ;;
        nov* ) days=30    ;; dec* ) days=31    ;;
        * ) echo "$0: Unknown month name $1">&2
            exit 1
    esac
    if [ $2 -lt 1 -o $2 -gt $days ] ; then
        return 1
    else
        return 0    # The day number is valid.
    fi
}
isLeapYear() {
    # This function returns 0 if the specified year is a leap year;
    #    1 otherwise.
    # The formula for checking whether a year is a leap year is:
    #    1. Years not divisible by 4 are not leap years.
    #    2. Years divisible by 4 and by 400 are leap years.
    #    3. Years divisible by 4, not divisible by 400, but divisible
    #       by 100 are not leap years.
    #    4. All other years divisible by 4 are leap years.
    
    year=$1
    if [ "$((year % 4))" -ne 0 ] ; then
        return 1 # Nope, not a leap year.
    elif [ "$((year % 400))" -eq 0 ] ; then
        return 0 # Yes, it's a leap year.
    elif [ "$((year % 100))" -eq 0 ] ; then
        return 1
    else
        return 0
    fi
}


echon() {
    echo -e "$*" | tr -d '\n'
}


initializeANSI() {
    esc="\033"  # If this doesn't work, enter an ESC directly.


    # Foreground colors
    blackf="${esc}[0;30m"
    yellowf="${esc}[0;33m"
    cyanf="${esc}[0;36m"
    redf="${esc}[0;31m"
    bluef="${esc}[0;34m"
    whitef="${esc}[0;37m"
    greenf="${esc}[0;32m"
    purplef="${esc}[0;35m"
    
    # Background colors
    blackb="${esc}[0;40m"
    yellowb="${esc}[0;43m"
    cyanb="${esc}[0;46m"
    redb="${esc}[0;41m"
    blueb="${esc}[0;44m"
    whiteb="${esc}[0;47m"
    greenb="${esc}[0;42m"
    purpleb="${esc}[0;45m"
    
    # Bold, italic, underline, and inverse style toggles
    boldon="${esc}[1m";    boldoff="${esc}[22m"
    italicson="${esc}[3m"; italicsoff="${esc}[23m"
    ulon="${esc}[4m";      uloff="${esc}[24m"
    invon="${esc}[7m";     invoff="${esc}[27m"
    reset="${esc}[0m"
}

# PROGRAMA QUE DEPENDE DE 12 Y POR SI SOLO NO HACE NADA PERO ES PARA QUE PUEDA FUNCIONAR VARIAS FUNCIONES DE OTROS SCRIPTS
