# Prerequisitos en este Tema

## Creando la imagen y Corriendo el contendor

En esta sección para probar los scripts he necesitado crearme una imagen Docker con Apache
que es el Dockerfile que hay en esta carpeta. 
con el comando
```bash
 docker build -t apache-cgi .
```
 generando así la imagen que usaré 
para el contenedor, luego para ejecutar el contenedor
```bash
docker run -i -t -p "80:80" --name apache-cgi-contendor \
-v ./recursos/apache_server:/var/www/html apache-cgi:latest
```

## Modificando la imagen Base de Ubuntu

- Luego para ejecutar scripts en apache he tenido que modificar el contenedor para ello
he acoplado una terminal interactiva 
```bash
docker exec -it apache-cgi-contendor /bin/bash
```
- Acto seguido he añadido la siguiente carpeta en `/var/www/html` llamada `cgi`
donde pondré ahí mis cgi para que se ejecuten y luego he modificado el archivo
`/etc/apache2/conf-available/serve-cgi-bin.conf` quedando de la siguiente manera
```xml
<IfModule mod_alias.c>
        <IfModule mod_cgi.c>
                Define ENABLE_USR_LIB_CGI_BIN
        </IfModule>

        <IfModule mod_cgid.c>
                Define ENABLE_USR_LIB_CGI_BIN
        </IfModule>

        <IfDefine ENABLE_USR_LIB_CGI_BIN>
                ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
                ScriptAlias /cgi-bin/ /var/www/html/cgi/
                <Directory "/usr/lib/cgi-bin">
                        AllowOverride None
                        Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
                        Require all granted
                </Directory>
                <Directory "/var/www/html/cgi">
                        AllowOverride None
                        Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
                        Require all granted
                        AddHandler cgi-script cgi sh bin pl
                </Directory>
        </IfDefine>
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```
- Finalmente quedaría detener y arrancar el servidor y acceder a la ruta de
nuestro archivo que deberemos alojar en el volumen previamente creado para ello.

- Mirar a la hora de alojarlo que tenga permisos de ejecución el script y la carpeta que cree

_Fuente: https://www.cyberciti.biz/faq/my-scripts-in-cgi-bin-directory-not-working-how-do-i-troubleshoot-this-problem/_
