#!/bin/bash

echo "Content-type: text/html"
echo ""


echo "<html><body bgcolor=\"white\"><h2>CGI Runtime Environment</h2>"
echo "<pre>"
env || printenv
echo "</pre>"
echo "<h3>Input stream is:</h3>"
echo "<pre>"
cat -
echo "(end of input stream)</pre></body></html>"

exit 0

# SCRIPT SENCILLO QUE NOS MUESTRA COMO EJECUTAR EN UN SERVIDOR APACHE UN SCRIPT
# DE BASH QUE NOS MUESTRA LAS VARIABLES DE ENTORNO DEL SISTEMA Y ALGUNA ETIQUETA
# DE ENCABEZADO <H1> NOS SALE PARA MI EJEMPLO:

# CGI Runtime Environment

# HTTP_ACCEPT_ENCODING=gzip, deflate, br
# SERVER_NAME=localhost
# SCRIPT_NAME=/cgi/script63.cgi
# GATEWAY_INTERFACE=CGI/1.1
# HTTP_SEC_GPC=1
# SERVER_SOFTWARE=Apache/2.4.52 (Ubuntu)
# DOCUMENT_ROOT=/var/www/html
# HTTP_UPGRADE_INSECURE_REQUESTS=1
# PWD=/var/www/html/cgi
# REQUEST_URI=/cgi/script63.cgi
# SERVER_SIGNATURE=
# Apache/2.4.52 (Ubuntu) Server at localhost Port 80


# REQUEST_SCHEME=http
# QUERY_STRING=
# HTTP_ACCEPT_LANGUAGE=es,en;q=0.7,en-US;q=0.3
# HTTP_SEC_FETCH_DEST=document
# CONTEXT_DOCUMENT_ROOT=/var/www/html
# HTTP_ACCEPT=text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
# REMOTE_PORT=47398
# SERVER_ADMIN=webmaster@localhost
# HTTP_HOST=localhost
# HTTP_SEC_FETCH_USER=?1
# HTTP_SEC_FETCH_SITE=same-origin
# HTTP_CONNECTION=keep-alive
# SERVER_ADDR=172.17.0.2
# HTTP_DNT=1
# HTTP_USER_AGENT=Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/112.0
# CONTEXT_PREFIX=
# SHLVL=1
# HTTP_SEC_FETCH_MODE=navigate
# HTTP_REFERER=http://localhost/cgi/
# SERVER_PROTOCOL=HTTP/1.1
# SERVER_PORT=80
# SCRIPT_FILENAME=/var/www/html/cgi/script63.cgi
# REMOTE_ADDR=172.17.0.1
# PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# REQUEST_METHOD=GET
# _=/usr/bin/env

# Input stream is:

# (end of input stream)